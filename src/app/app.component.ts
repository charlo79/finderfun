import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
    { title: 'Perfil', url: '/folder/Inbox', icon: 'person-circle' },
    { title: 'Mis Cuentas', url: '/folder/Outbox', icon: 'play-circle' },
    { title: 'Inicio', url: '/folder/Favorites', icon: 'home' },
    { title: 'Metodos De Pago', url: '/folder/Archived', icon: 'card' },
    { title: 'Comentarios/Sugerencias', url: '/folder/Trash', icon: 'chatbox-ellipses' },
    { title: 'Cerrar Sesión', url: '/folder/Spam', icon: 'power' },
  ];
  public labels = ['Family', 'Friends', 'Notes', 'Work', 'Travel', 'Reminders'];
  

}
