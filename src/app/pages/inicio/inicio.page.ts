import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage implements OnInit {
  public folder: string;

 
  constructor(private activatedRoute: ActivatedRoute, private menuCtrl: MenuController, private navCtrl: NavController) {

    this.menuCtrl.enable(false); 
 
   }


   goToPaginaLogin() {

    setTimeout(() => {
    }, 700);
  }

  
  ngOnInit() {
    this.folder = this.activatedRoute.snapshot.paramMap.get('id');
  }

}

