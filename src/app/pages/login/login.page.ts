import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  
  constructor(private activatedRoute: ActivatedRoute, private menuCtrl: MenuController, private navCtrl: NavController) {

    this.menuCtrl.enable(false);  
   }

   goToPaginaIniciarSesion() {
      setTimeout(() => {
        this.navCtrl.navigateForward('/home');
      }, 700);
    }

    ngOnInit() {
    }

    showpassword(){

      var password1 :any =document.getElementById("contraseña");
      password1.childNodes[1].type="text"

      document.getElementById("passhide").style.display="";
      document.getElementById("passshow").style.display="none";

    };

    hidepassword(){

      var password1 :any =document.getElementById("contraseña");
      password1.childNodes[1].type="password"

      document.getElementById("passshow").style.display="";
      document.getElementById("passhide").style.display="none";

    };
}
