import { Component, OnInit } from '@angular/core';
import {ViewEncapsulation} from '@angular/core';


@Component({
  selector: 'app-seleccion',
  templateUrl: './seleccion.page.html',
  styleUrls: ['./seleccion.page.scss'],
  encapsulation: ViewEncapsulation.None,

})
export class SeleccionPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
