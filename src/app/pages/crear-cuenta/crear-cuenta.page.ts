import { Component, OnInit } from '@angular/core';

import { ActivatedRoute } from '@angular/router';
import {ViewEncapsulation} from '@angular/core';
import { MenuController } from '@ionic/angular';


@Component({
  selector: 'app-crear-cuenta',
  templateUrl: './crear-cuenta.page.html',
  styleUrls: ['./crear-cuenta.page.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CrearCuentaPage implements OnInit {


  constructor(private activatedRoute: ActivatedRoute, private menuCtrl: MenuController) {

    this.menuCtrl.enable(false);  
   }


  ngOnInit() {
  }

  showpassword(){

    var password1 :any =document.getElementById("contraseña");
    password1.childNodes[1].type="text"

    document.getElementById("passhide").style.display="";
    document.getElementById("passshow").style.display="none";

  };

  hidepassword(){

    var password1 :any =document.getElementById("contraseña");
    password1.childNodes[1].type="password"

    document.getElementById("passshow").style.display="";
    document.getElementById("passhide").style.display="none";

  };

  
}
