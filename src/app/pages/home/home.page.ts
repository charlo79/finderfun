import { Component, OnInit } from '@angular/core';
import {ViewEncapsulation} from '@angular/core';

import { ActivatedRoute } from '@angular/router';
import { MenuController } from '@ionic/angular';


@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class HomePage implements OnInit {
  navCtrl: any;

  constructor(private activatedRoute: ActivatedRoute, private menuCtrl: MenuController) {

    this.menuCtrl.enable(true); 
     

  }



  ngOnInit() {
  }

}
